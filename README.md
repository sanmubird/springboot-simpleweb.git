# springboot-simpleweb

#### 项目介绍
使用 springboot  freemark 搭建 简单的web项目,  包含 测试用例 和  热修复设置。
##### 1 springboot yml 配置
##### 2 springboot 和 freeamark 搭配使用
> 页面跳转：分别使用了 RestController 注解 和 Controller 注解
  值传递： 也分别尝试了  RestController 注解 和 Controller 注解

##### 3  热修复：
> 1）： 尝试了 修复方法体；   
  2）： 尝试热新增 方法；  
  3）： 尝试了热新增类；    
  以上 均可实现： 实现方式：依赖 + 配置

##### 4 测试用例：
> 使用 mock  Mock 出 mvc 控制层 进行请求转发：