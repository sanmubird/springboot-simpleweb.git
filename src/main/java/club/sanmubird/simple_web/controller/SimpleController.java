package club.sanmubird.simple_web.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @program: simple_web
 * @description: 简单的controller
 * @author: sam
 * @create: 2018-07-15 16:36
 **/
@RestController
@RequestMapping("/simple_web")
public class SimpleController {

    @RequestMapping("/hello")
    String hello() {
        return "Hello World!";
    }

	@RequestMapping("/ok")
	String ok() {
		return "ok";
	}

	@RequestMapping("/new")
	String xinjian() {
		return "new method";
	}
}
