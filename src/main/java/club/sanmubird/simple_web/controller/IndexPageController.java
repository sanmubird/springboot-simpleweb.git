package club.sanmubird.simple_web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;

/**
 * @Controller 类中的方法
 * 可以直接通过返回String跳转到jsp、ftl、html等模版页面。
 * 在方法上加@ResponseBody注解，也可以返回实体对象。
 * @RestController类中的所有方法 只能返回String、Object、Json等实体对象，不能跳转到模版页面。
 * 相当于@ResponseBody + @Controller。
 * @RestController中的方法 如果想跳转页面，则用ModelAndView进行封装.
 * 例子见： club.sanmubird.simple_web.controller.TestRestController
 */

/**
 * @program: simple_web
 * @description: 这个是首页的controller
 * @author: sam
 * @create: 2018-07-15 19:31
 **/
@Controller
@RequestMapping("/common")
public class IndexPageController {

	@RequestMapping("/index")
	public String firstPage() {
		return "index";
	}

	@RequestMapping("/data")
	public String dataPage(ModelMap map) {
		//ModelMap转发值的作用
		map.addAttribute("name","哈喽世界");
		map.put("sex", 1);
		List<String> userList = new ArrayList<String>();
		userList.add("张三");
		userList.add("李四");
		userList.add("王五");
		map.addAttribute("userList",userList);
		return "dataPage";
	}

}
