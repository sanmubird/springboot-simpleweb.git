package club.sanmubird.simple_web.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @program: simple_web
 * @description: 测试 restController  是否可以 跳转到模板
 * @author: sam
 * @create: 2018-07-15 19:53
 **/
@RestController
@RequestMapping("/rest")
public class TestRestController {

	@RequestMapping("/index")
	public ModelAndView toIndex() {
		ModelAndView mv = new ModelAndView("index");
		return mv;
	}

	@RequestMapping("/data")
	public ModelAndView freemarker() {
		ModelAndView mv = new ModelAndView("dataPage");
		Map map = mv.getModel();
		map.put("name","世界—rest");
		map.put("sex", 1);
		List<String> userList = new ArrayList<String>();
		userList.add("张三-rest");
		userList.add("李四-rest");
		userList.add("王五-rest");
		map.put("userList",userList);
		mv.addAllObjects(map);
		return mv;
	}
}
