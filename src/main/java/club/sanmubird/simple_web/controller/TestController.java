package club.sanmubird.simple_web.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @program: simple_web
 * @description: 这个类是在运行期间创建的，目的是测试springboot的热编译热部署热启动能力
 * @author: sam
 * @create: 2018-07-15 17:43
 **/
@RestController
@RequestMapping("/new")
public class TestController {

	@RequestMapping("/method")
	public  String newMethod(){
		return "great ! build when run ! 666!";
	}
}
